module Api
  class EventsController < CrudController
    self.permitted_attrs = ['name']
  end
end
