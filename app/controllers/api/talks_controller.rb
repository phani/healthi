class Api::TalksController < Api::CrudController
  before_action :set_event_id, only: :create
  self.permitted_attrs = ['id', 'name', 'event_id', 'duration']
  self.nesting = Event

  def allocated_talks
    alloted_tracks = []
    talks = Event.find(params[:event_id]).talks
    while talks.count > 0 do 
      track = set_track talks 
      alloted_tracks.push(track[:talks_alloted])
      talks = talks.where("id NOT IN (?)", track[:allocated_talks_ids])
    end
    render json: {status: "success", status_code: 200, data: alloted_tracks}
  end

  private

  def set_track talks
    allocated_talks_ids = []
    talks_alloted = {}
    talks_duration = talks.inject(0) {|s, track| s + track.duration}
    talks_duration = talks_duration < 180 ? talks_duration : 180
    
    # Generate for first half 9 - 12 = 180minutes
    morning_talks = get_alloted_talks talks, talks_duration
    allocated_talks_ids = morning_talks.pluck(:id)
    talks_alloted['morning_talks'] = set_time_to_talks morning_talks, 9, false

    remaining_talks = talks.where("id NOT IN (?)", allocated_talks_ids)
    talks_duration = remaining_talks.inject(0) {|s, track| s + track.duration}
    # if talks duration is more than 240 have max duration of evening track as 240, else check for 210
    talks_duration = talks_duration < 240 ? (talks_duration < 210 ? talks_duration : 210) : 240
    
    # need to generate for next half 1 - 4.30 = 210minutes or till 5 240minutes
    evening_talks = get_alloted_talks remaining_talks, talks_duration
    allocated_talks_ids = allocated_talks_ids + evening_talks.pluck(:id)
    talks_alloted['evening_talks'] = set_time_to_talks evening_talks, 13, true
    {talks_alloted: talks_alloted, allocated_talks_ids: allocated_talks_ids}
  end

  def get_alloted_talks talks, duration
    talks_generator = TalksAllotmentGenerator.new
    talks_generator.generate talks, duration
    # take any one of the alloted talks, as they where many(1001) possibilties
    # for given input.
    talks_generator.alloted_talks.compact.sample
  end

  def set_time_to_talks talks, start_hour, add_networking
    alloted_talks = []
    if talks
      start_time = DateTime.now.change({ hour: start_hour, min: 0, sec: 0 })
      talks.each do |track|
        track = track.attributes
        track['start_time'] = "#{start_time.strftime("%I:%M %p")}"
        alloted_talks.push(track)
        start_time = start_time + track['duration'].minutes
      end
      if add_networking
        network_min_start_time = DateTime.now.change({ hour: 16, min: 0, sec: 0 })
        network_max_end_time = DateTime.now.change({ hour: 17, min: 10, sec: 0 })
        if start_time < network_max_end_time
          # If start time for network event less than 4, make start time as 4.
          start_time = (start_time < network_min_start_time) ? network_min_start_time : start_time
          newtwork_track  = {name: 'Network Event', duration: 30, start_time: "#{start_time.strftime("%I:%M %p")}"}
          alloted_talks.push(newtwork_track)
        end        
      end
    end
    alloted_talks
  end

  def set_event_id
    params['talk']['event_id'] = params['event_id']
  end

end