class TalksAllotmentGenerator

  attr_accessor :alloted_talks

  def initialize
    @alloted_talks = []
  end

  # each time partial will be having the talks till previous allotment
  def generate talks, target_duration, partial=[]
    sum = partial.inject(0) {|s, track| s + (track.duration)} || 0

    # check if the partial sum is equals to target duration
    if sum == target_duration 
      @alloted_talks.push(partial)
    end

    if sum >= target_duration
      return
    end

    for i in 0..(talks.length)
      n = talks[i]
      if i < talks.length
        remaining = talks[i+1..-1]
        generate(remaining, target_duration, partial + [n])
      end
    end 
  end

end
