app.controller('TalksController', ['$http', '$routeParams',
    function($http, $routeParams) {
  var eventId = $routeParams.eventId;
  this.track = {name: '', duration: null, event_id: eventId};
  this.talks = [];

  $http.get('/api/events/' + eventId + '/talks').then(function(res) {
    this.talks = res.data.data;
  }.bind(this));

  this.create = function() {
    $http.post('/api/events/' + eventId + '/talks', {talk: this.track}).then(function(res) {
      this.talks.push(res.data.data);
    }.bind(this));
  }; 

  this.getallotedTracks = function() {
    $http.post('/api/events/' + eventId + '/allocated_talks').then(function(res) {
      this.allotedTracks = res.data.data;
    }.bind(this));
  };

  this.onDurationChanged = function() {
    if (this.track.duration > 60) {
      this.track.duration = 60;
    }
  }
}]);
