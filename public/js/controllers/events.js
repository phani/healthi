app.controller('EventsController', ['$http', function($http) {
  this.event = {name: ''};
  this.events = [];

  $http.get('/api/events').then(function(res) {
    this.events = res.data.data;
  }.bind(this));

  this.create = function() {
    $http.post('/api/events', {event: this.event}).then(function(res) {
      this.events.push(res.data.data);
    }.bind(this));
  }; 
}]);
