var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "templates/events.html",
        controller: 'EventsController',
        controllerAs: 'eventsController'
    })
    .when("/:eventId/talks", {
        templateUrl : "templates/talks.html",
        controller: 'TalksController',
        controllerAs: 'talksController'
    })
});
