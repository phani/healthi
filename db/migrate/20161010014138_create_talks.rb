class CreateTalks < ActiveRecord::Migration[5.0]
  def change
    create_table :talks do |t|
      t.string :name
      t.integer :duration
      t.references :event
    end
  end
end
