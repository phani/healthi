# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: ''Star Wars' }, { name: ''Lord of the Rings' }])
#   Character.create(name: ''Luke', movie: movies.first)

event = Event.create({name: 'Tech Talks'})

talks = [
  {name: 'Writing Fast Tests Against Enterprise Rails', duration: 60},

  {name: 'Overdoing it in Python', duration: 45},

  {name: 'Lua for the Masses', duration: 30},

  {name: 'Ruby Errors from Mismatched Gem Versions', duration: 45},

  {name: 'Common Ruby Errors', duration: 45},

  {name: 'Rails for Python Developers', duration: 5}, # lightening

  {name: 'Communicating Over Distance', duration: 60},

  {name: 'Accounting-Driven Development', duration: 45},

  {name: 'Woah', duration: 30},

  {name: 'Sit Down and Write', duration: 30},

  {name: 'Pair Programing vs Noise', duration: 45},

  {name: 'Rails Magic', duration: 60},

  {name: 'Ruby on Rails: Why We Should Move On', duration: 60},

  {name: 'Clojure Ate Scala (on my project)', duration: 45},

  {name: 'Programing in the Boondocks of Seattle', duration: 30},

  {name: 'Ruby vs. Clojure for Back-End Development', duration: 30},

  {name: 'Ruby on Rails Legacy App Maintenance', duration: 60},

  {name: 'A World Without HackerNews', duration: 30},

  {name: 'User Interface CSS in Rails Apps', duration: 30}
]

talks.each do |talk|
  talk['event_id'] = event.id
  Talk.create talk
end

